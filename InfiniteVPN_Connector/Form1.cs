﻿using Newtonsoft.Json.Linq;
using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Forms;

namespace InfiniteVPN_Connector
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            CheckForIllegalCrossThreadCalls = false;

        }

        string server;

        private static string FolderPath => string.Concat(Directory.GetCurrentDirectory(), "\\VPN");

        public void info()
        {
            Thread thread = new Thread(() =>
            {
                JObject jobject = JObject.Parse(new WebClient().DownloadString("http://ip-api.com/json/"));
                lblIP.Text = "IP : " + (string)jobject["query"];
                lblLoc.Text = "Location : " + (string)jobject["country"];
                WebClient wc = new WebClient();
                string flagAPI = "https://www.countryflags.io/" + (string)jobject["countryCode"] + "/flat/64.png";
                string src = wc.DownloadString(flagAPI);
                string aks = "";
                aks = flagAPI + Regex.Match(src, "<img src=\".(.*?)\" alt=\".(.*?)\" class=\"transparent\">").Groups[1].Value;
                pictureBox.ImageLocation = aks;
            }); thread.Start();
        }

        private void btnConnect_Click(object sender, EventArgs e)
        {
            if (btnConnect.Text == "Connect")
            {
                if (!Directory.Exists(FolderPath))
                    Directory.CreateDirectory(FolderPath);
                server = txtServer.Text;
                var sb = new StringBuilder();
                sb.AppendLine("[VPN]");
                sb.AppendLine("MEDIA=rastapi");
                sb.AppendLine("Port=VPN2-0");
                sb.AppendLine("Device=WAN Miniport (IKEv2)");
                sb.AppendLine("DEVICE=vpn");
                sb.AppendLine("PhoneNumber=" + server);
                ConnTimer.Enabled = true;
                File.WriteAllText(FolderPath + "\\VpnConnection.pbk", sb.ToString());

                sb = new StringBuilder();
                sb.AppendLine("rasdial \"VPN\" " + txtUsrname.Text + " " + txtPassword.Text + " /phonebook:\"" + FolderPath +
                              "\\VpnConnection.pbk\"");

                File.WriteAllText(FolderPath + "\\VpnConnection.bat", sb.ToString());

                var newProcess = new Process
                {
                    StartInfo =
                {
                    FileName = FolderPath + "\\VpnConnection.bat",
                    WindowStyle = ProcessWindowStyle.Normal
                }
                };

                newProcess.Start();
                newProcess.WaitForExit();
                btnConnect.Text = "Disconnect";
                Thread.Sleep(500);
                info();

            }
            else if (btnConnect.Text == "Disconnect")
            {
                File.WriteAllText(FolderPath + "\\VpnDisconnect.bat", "rasdial /d");

                var newProcess = new Process
                {
                    StartInfo =
                    {
                    FileName = FolderPath + "\\VpnDisconnect.bat",
                    WindowStyle = ProcessWindowStyle.Normal
                    }
                };
                ConnTimer.Enabled = false;
                newProcess.Start();
                newProcess.WaitForExit();
                btnConnect.Text = "Connect";
                info();

            }
        }
        int sec = 0; int min = 0;
        private void ConnTimer_Tick(object sender, EventArgs e)
        {
            sec++;
            if (sec == 60) { min++; sec = 0; }
            lblCT.Text = "Connection Time : " + min + "m:" + sec + "s";
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            info();
        }

        private void Form1_Shown(object sender, EventArgs e)
        {

        }
    }
}